module.exports = function override(config, env) {
  //do stuff with the webpack config...
  config.resolve.fallback = {
    ...config.resolve.fallback,
    "crypto": false,
    "crypto-browserify": require.resolve('crypto-browserify'),
    "util": require.resolve("util/")
  };
  return config;
}