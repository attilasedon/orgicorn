import { KeyboardEvent, useEffect, useState } from "react";

type Piece = {
  source: 'original' | 'add',
  start: number,
  end: number
}

type PieceTableElement = {
  original: string,
  add?: string,
  pieces: Piece[]
}

type PieceTable = PieceTableElement[]

export function Editor() {
  const [pieceTable, setTable] = useState<PieceTable>([{
    original: "Hello",
    pieces: []
  }]);
  const [currentState, setCurrentState] = useState<number>(0);
  // Check if using useState here could cause lag. 
  const [edit, setEdit] = useState<PieceTableElement>({
    original: showCurrentText(),
    pieces: [
      { source: "original", start: 0, end: showCurrentText().length }
    ]
  });

  useEffect(() => {
    console.log('pieceTable: ', pieceTable);
  }, [pieceTable])

  // Maybe this needs a more complex data structure
  const [cursorPosition, setCursorPosition] = useState<number>(0);
  const addChange = (piece: PieceTableElement) => {
    setTable(current => ([...current, piece]))
  }

  function applyChanges(pieceTable: PieceTableElement): string {
    return pieceTable.pieces.reduce((dest, piece) => {
      return dest + pieceTable[piece.source]?.slice(piece.start, piece.end)
    }, '');
  }

  function showCurrentText(): string {
    return applyChanges(pieceTable[currentState]);
  }

  useEffect(() => {
    const timeOutId = setTimeout(() => {
      if (edit.add) {
        return addChange(edit);
      }
    }, 1000);
    return () => clearTimeout(timeOutId);
  }, [edit]);

  const handleChange = (event: KeyboardEvent) => {
    // Letter branch
    console.log(event.key);
    if (!edit.add) {
      edit.add = '';
    }
    const newAdd = edit.add + event.key;
    const newCursorPosition = cursorPosition + 1;
    setCursorPosition(newCursorPosition);
    const editStart = newCursorPosition - newAdd.length;
    const newEdit: PieceTableElement = {
      original: showCurrentText(),
      add: newAdd,
      pieces: [
        { source: "original", start: 0, end: editStart },
        { source: "add", start: editStart, end: newCursorPosition },
        { source: "original", start: newCursorPosition, end: showCurrentText.length }
      ]
    }
    setEdit(newEdit);
  }

  return (
    <div>
      <textarea defaultValue={showCurrentText()} onKeyDown={handleChange} />
    </div>
  );

}
