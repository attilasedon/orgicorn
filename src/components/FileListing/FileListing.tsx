import { files } from 'dropbox';

export type DropBoxFileType = (files.FileMetadataReference | files.FolderMetadataReference | files.DeletedMetadataReference);

export type FileListingProps = {
  files: DropBoxFileType[];
}


export default function FileListing({ files }: FileListingProps) {
  console.log('files: ', files);
  return (
    <div className="flex flex-col h-screen">
      <header className="flex items-center justify-between p-4 bg-gray-800 text-white">
        <h1 className="text-lg font-semibold">File Selector</h1>
        <div className="relative w-64">
          <input
            className="w-full h-10 px-3 rounded-md border border-gray-600 placeholder-gray-500 text-sm focus:outline-none"
            placeholder="Search files..."
            type="text"
          />
          <button className="absolute inset-y-0 right-0 flex items-center px-3 border-l border-gray-600">
            <svg
              className="h-5 w-5 text-gray-500"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M21 21l-6-6m2-6a7 7 0 11-14 0 7 7 0 0114 0z"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
              />
            </svg>
          </button>
        </div>
      </header>
      <main className="flex-1 overflow-auto p-4 space-y-4">
        <h2 className="text-lg font-semibold">Select Files</h2>
        <div className="flex flex-col gap-4">
          {files.map(f => {
            return (<div className="border border-gray-300 rounded-lg p-4">
              <h3 className="font-medium">{f.name}</h3>
              <p className="text-sm text-gray-500">
                <i className="fas fa-folder" />
                Type: {f['.tag']}{"\n                      "}
              </p>
            </div>);
          })}
        </div>
      </main>
    </div>
  )
}
