import { Dropbox, DropboxAuth } from 'dropbox';
import { useEffect, useState, useMemo } from 'react';

function getCodeFromUrl(): string | null {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get('code');
}

// If the user was just redirected from authenticating, the urls hash will
// contain the access token.
function hasRedirectedFromAuth(): boolean {
  return !!getCodeFromUrl();
}

export function useDropbox() {
  console.log('render');
  const [dbx, setDbx] = useState<Dropbox | null>(null);
  const REDIRECT_URI = window.location.origin + '/';
  // TODO: Use it from actual .env file
  // const CLIENT_ID = process.env.REACT_APP_DROPBOX_CLIENT_ID;
  const dbxAuth = useMemo(() => new DropboxAuth({
    clientId: "nxkkazd3401f4yr",
  }), []);

  const savedRefreshToken = window.sessionStorage.getItem('refreshToken');
  const savedAccessToken = window.sessionStorage.getItem('accessToken');

  useEffect(() => {
    // Set from saved tokens
    if (savedAccessToken && savedRefreshToken) {
      dbxAuth.setAccessToken(savedAccessToken || '');
      dbxAuth.setRefreshToken(savedRefreshToken || '');
      setDbx(new Dropbox({
        auth: dbxAuth
      }));
    // set from redirection
    } else if (hasRedirectedFromAuth()) {
      dbxAuth.setCodeVerifier(window.sessionStorage.getItem('codeVerifier') || '');
      dbxAuth.getAccessTokenFromCode(REDIRECT_URI, getCodeFromUrl() || '')
        .then((response) => {
          const { refresh_token, access_token } = response.result as { access_token: string, refresh_token: string };
          dbxAuth.setAccessToken(access_token);
          window.sessionStorage.setItem('accessToken', access_token);
          dbxAuth.setRefreshToken(refresh_token);
          window.sessionStorage.setItem('refreshToken', refresh_token);
          setDbx(new Dropbox({
            auth: dbxAuth
          }));
          window.location.href = window.location.origin;
        });
    // set start redirection
    } else {
      dbxAuth.getAuthenticationUrl(REDIRECT_URI, undefined, 'code', 'offline', undefined, undefined, true)
        .then(authUrl => {
          window.sessionStorage.clear();
          window.sessionStorage.setItem("codeVerifier", dbxAuth.getCodeVerifier());
          window.location.href = authUrl as string;
        })
        .catch((error) => console.error(error));
    }
  }, []);

  return [dbx];
}
