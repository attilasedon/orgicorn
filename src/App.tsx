import logo from './logo.svg';
import './App.css';
import { useDropbox } from './hooks/useDropbox';
import { useEffect, useState } from 'react';
import FileListing, { DropBoxFileType } from './components/FileListing/FileListing';
import { Editor } from './components/Editor/Editor';

function App() {

  const [files, setFiles] = useState<DropBoxFileType[]>([])

  const [dbx] = useDropbox();

  useEffect(() => {
    dbx?.filesListFolder({
      path: ''
    }).then(resp => {
      setFiles(resp.result.entries);
    }).catch(e => {
      console.log(e);
    });
  }, [dbx]);

  return (
    <div className="App">
      <Editor />
      <FileListing files={files} />
    </div>
  );
}

export default App;
